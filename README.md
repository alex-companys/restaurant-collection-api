# Restaurant Collection API

Restaurant Collection API is a service that provides details of restaurants spread across multiple cities. It supports various operations such as adding, updating, deleting restaurants, fetching restaurant details, listing restaurants in a particular city, and sorting restaurants by user rating.

## Table of Contents
- [Getting Started](#getting-started)
- [API Endpoints](#api-endpoints)
- [Contributing](#contributing)
- [License](#license)

## Getting Started

### Prerequisites
- Java 17
- Maven
- PostgreSQL
- Docker

### Running the Application Locally
1. Clone the repository: `git clone git@gitlab.com:alex-companys/restaurant-collection-api.git`
2. Navigate to the project directory: `cd restaurant-collection-api`
3. Set up PostgreSQL database and update application.properties with the database configuration.
4. Run the application: `./mvnw spring-boot:run`
5. The application will be available at `http://localhost:8080`.

### Setting Up the Database


- This application uses a PostgreSQL database. You can set up the database using the provided `docker-compose.yaml` file. To start the database, run the following command in the project directory:

```bash
docker-compose up
```

### API Endpoints

- **Add a new restaurant**
  - Endpoint: `POST /restaurant`
  - Request Body: JSON object with restaurant details
  - Response: 201 Created
  - Example:
    POST http://localhost:8080/restaurant \
    Content-Type: application/json'
    
    {
    "city": "Miami",
    "name": "Byg Company",
    "estimatedCost": 1600,
    "averageRating": "4.9",
    "votes": "16203"
    }
  
- **Update restaurant rating**
  - Endpoint: `PUT /restaurant/{id}`
  - Request Parameters: `averageRating` and `votes`
  - Response: 200 OK
  - Example: PUT http://localhost:8080/restaurant/1?averageRating=55&votes=55  

- **Get all restaurants**
  - Endpoint: `GET /restaurant`
  - Response: 200 OK with a list of all restaurants

- **Get all restaurants in a particular city**
  - Endpoint: `GET /restaurant/query-by-city?city={city}`
  - Response: 200 OK with a list of restaurants in the specified city
  - Example: http://localhost:8080/restaurant/query-by-city?city=Miami

- **Get restaurant by id**
  - Endpoint: `GET /restaurant/query?id={id}`
  - Response: 200 OK with details of the specified restaurant

- **Delete restaurant by id**
  - Endpoint: `DELETE /restaurant/{id}`
  - Response: 204 No Content

- **Sort restaurants by rating**
  - Endpoint: `GET /restaurant/sort`
  - Response: 200 OK with a list of restaurants sorted by rating

## Contributing

If you'd like to contribute to this project, please fork the repository and submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
