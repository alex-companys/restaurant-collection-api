package com.andersen.restaurant.repository;

import com.andersen.restaurant.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    List<Restaurant> findByCity(String city);

    @Query(value = "SELECT * FROM restaurant ORDER BY CAST(average_rating AS DECIMAL) DESC", nativeQuery = true)
    List<Restaurant> findAllOrderByRatingDesc();
}
