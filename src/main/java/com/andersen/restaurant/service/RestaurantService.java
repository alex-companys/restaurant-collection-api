package com.andersen.restaurant.service;

import com.andersen.restaurant.exception.RestaurantNotFoundException;
import com.andersen.restaurant.model.Restaurant;
import com.andersen.restaurant.repository.RestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RestaurantService {

    private final RestaurantRepository restaurantRepository;

    public Restaurant addRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    public Restaurant updateRating(Long id, String averageRating, Integer votes) {
        Restaurant restaurant = restaurantRepository.findById(id)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant not found with id: " + id));

        restaurant.setAverageRating(averageRating);
        restaurant.setVotes(votes);
        return restaurantRepository.save(restaurant);
    }

    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.findAll();
    }

    public List<Restaurant> getRestaurantsByCity(String city) {
        List<Restaurant> restaurants = restaurantRepository.findByCity(city);
        if (restaurants.isEmpty()) {
            throw new RestaurantNotFoundException("No restaurants found in city: " + city);
        }
        return restaurants;
    }

    public Restaurant getRestaurantById(Long id) {
        return restaurantRepository.findById(id)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant not found with id: " + id));
    }

    public void deleteRestaurant(Long id) {
        restaurantRepository.deleteById(id);
    }

    public List<Restaurant> sortRestaurantsByRating() {
        return restaurantRepository.findAllOrderByRatingDesc();
    }
}
