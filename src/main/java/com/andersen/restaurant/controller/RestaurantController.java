package com.andersen.restaurant.controller;

import com.andersen.restaurant.model.Restaurant;
import com.andersen.restaurant.service.RestaurantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/restaurant")
public class RestaurantController {

    private final RestaurantService restaurantService;

    @PostMapping
    public ResponseEntity<Restaurant> addRestaurant(@RequestBody Restaurant restaurant) {
        Restaurant addedRestaurant = restaurantService.addRestaurant(restaurant);
        return new ResponseEntity<>(addedRestaurant, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Restaurant> updateRating(
            @PathVariable Long id,
            @RequestParam String averageRating,
            @RequestParam Integer votes
    ) {
        Restaurant updatedRestaurant = restaurantService.updateRating(id, averageRating, votes);
            return new ResponseEntity<>(updatedRestaurant, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Restaurant>> getAllRestaurants() {
        List<Restaurant> restaurants = restaurantService.getAllRestaurants();
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @GetMapping("/query-by-city")
    public ResponseEntity<List<Restaurant>> getRestaurantsByCity(@RequestParam String city) {
        List<Restaurant> restaurants = restaurantService.getRestaurantsByCity(city);
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @GetMapping("/query")
    public ResponseEntity<Restaurant> getRestaurantById(@RequestParam Long id) {
        Restaurant restaurant = restaurantService.getRestaurantById(id);
            return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRestaurant(@PathVariable Long id) {
        restaurantService.deleteRestaurant(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/sort")
    public ResponseEntity<List<Restaurant>> sortRestaurantsByRating() {
        List<Restaurant> sortedRestaurants = restaurantService.sortRestaurantsByRating();
        return new ResponseEntity<>(sortedRestaurants, HttpStatus.OK);
    }
}


