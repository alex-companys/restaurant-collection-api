package com.andersen.restaurant.service;

import com.andersen.restaurant.exception.RestaurantNotFoundException;
import com.andersen.restaurant.model.Restaurant;
import com.andersen.restaurant.repository.RestaurantRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class RestaurantServiceTest {
    @Mock
    private RestaurantRepository restaurantRepository;

    @InjectMocks
    private RestaurantService restaurantService;

    @Test
    void shouldAddRestaurant() {
        Restaurant restaurant = new Restaurant();
        when(restaurantRepository.save(restaurant)).thenReturn(restaurant);

        Restaurant result = restaurantService.addRestaurant(restaurant);

        assertNotNull(result);
        assertEquals(restaurant, result);
        verify(restaurantRepository, times(1)).save(restaurant);
    }

    @Test
    void shouldUpdateRating() {
        Long id = 1L;
        String averageRating = "4.5";
        Integer votes = 100;
        Restaurant restaurant = new Restaurant();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        when(restaurantRepository.save(restaurant)).thenReturn(restaurant);

        Restaurant result = restaurantService.updateRating(id, averageRating, votes);

        assertNotNull(result);
        assertEquals(averageRating, result.getAverageRating());
        assertEquals(votes, result.getVotes());
        verify(restaurantRepository, times(1)).findById(id);
        verify(restaurantRepository, times(1)).save(restaurant);
    }
    @Test
    void shouldGetAllRestaurants() {
        List<Restaurant> restaurantList = Arrays.asList(new Restaurant(), new Restaurant());
        when(restaurantRepository.findAll()).thenReturn(restaurantList);

        List<Restaurant> result = restaurantService.getAllRestaurants();

        assertNotNull(result);
        assertEquals(restaurantList.size(), result.size());
        verify(restaurantRepository, times(1)).findAll();
    }

    @Test
    void shouldGetRestaurantsByCity() {
        String city = "Miami";
        List<Restaurant> restaurantList = Arrays.asList(new Restaurant(), new Restaurant());
        when(restaurantRepository.findByCity(city)).thenReturn(restaurantList);

        List<Restaurant> result = restaurantService.getRestaurantsByCity(city);

        assertNotNull(result);
        assertEquals(restaurantList.size(), result.size());
        verify(restaurantRepository, times(1)).findByCity(city);
    }

    @Test
    void shouldThrowExceptionWhenNoRestaurantsInCity() {
        String city = "NonexistentCity";
        when(restaurantRepository.findByCity(city)).thenReturn(Collections.emptyList());

        assertThrows(RestaurantNotFoundException.class, () -> restaurantService.getRestaurantsByCity(city));
        verify(restaurantRepository, times(1)).findByCity(city);
    }

    @Test
    void shouldGetRestaurantById() {
        Long id = 1L;
        Restaurant restaurant = new Restaurant();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));

        Restaurant result = restaurantService.getRestaurantById(id);

        assertNotNull(result);
        assertEquals(restaurant, result);
        verify(restaurantRepository, times(1)).findById(id);
    }

    @Test
    void shouldThrowExceptionWhenRestaurantNotFoundById() {
        Long id = 1L;
        when(restaurantRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(RestaurantNotFoundException.class, () -> restaurantService.getRestaurantById(id));
        verify(restaurantRepository, times(1)).findById(id);
    }

    @Test
    void shouldDeleteRestaurant() {
        Long id = 1L;

        restaurantService.deleteRestaurant(id);

        verify(restaurantRepository, times(1)).deleteById(id);
    }

    @Test
    void shouldSortRestaurantsByRating() {
        List<Restaurant> restaurantList = Arrays.asList(new Restaurant(), new Restaurant());
        when(restaurantRepository.findAllOrderByRatingDesc()).thenReturn(restaurantList);

        List<Restaurant> result = restaurantService.sortRestaurantsByRating();

        assertNotNull(result);
        assertEquals(restaurantList.size(), result.size());
        verify(restaurantRepository, times(1)).findAllOrderByRatingDesc();
    }
}